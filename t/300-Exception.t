use Test::More;

use_ok("Zaaksysteem::Tools");

### ZS-15494: Test if throw works with non-true-stringified given object
{
    use_ok("NonTrueValue");
    my $nonvalue = NonTrueValue->new();

    eval {
        throw(
            "params/profile",
            "Checking of params profile object",
            $nonvalue
        );
    };

    isa_ok($@, "Zaaksysteem::Exception::Base");
    isa_ok($@->object, 'NonTrueValue');
}


done_testing();

package NonTrueValue;

use Moose;

use overload
    '""' => \&_stringify;

sub _stringify {
    return;
}

1;