FROM perl:latest
RUN cpanm --notest -i Module::Install

COPY . /opt/zaaksysteem-tools
WORKDIR /opt/zaaksysteem-tools

RUN perl Makefile.PL && cpanm --notest -i .