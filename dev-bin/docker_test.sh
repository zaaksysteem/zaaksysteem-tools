#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

cd $DIR;

if [ "$(docker images -a|grep registry.gitlab.com/zaaksysteem/zaaksysteem-tools)" != "" ]; then
    echo "Found existing docker image, no build"
else
    echo "Did not find existing image, building"
    docker build -t registry.gitlab.com/zaaksysteem/zaaksysteem-tools .
fi

if [ "$1" == "" ]; then
    docker run -v $DIR:/opt/zaaksysteem-tools \
        registry.gitlab.com/zaaksysteem/zaaksysteem-tools \
        prove -l t
else
    docker run -v $DIR:/opt/zaaksysteem-tools \
        registry.gitlab.com/zaaksysteem/zaaksysteem-tools \
        prove -l $@
fi