# Zaaksysteem Tools Library

[![build status](https://gitlab.com/zaaksysteem/zaaksysteem-tools/badges/master/build.svg)](https://gitlab.com/zaaksysteem/zaaksysteem-tools/commits/master)

This repository hosts common utilities used in the Zaaksysteem projects

## Docker development

For developers who do not want to pollute their host system, development based
on docker can be accomplished by using the following instructions

### Run testsuite

This will run the testsuite, and will only build the instance once (so you will
have all the dependencies) and every next run skips the build for faster testing.

**Basic**

```
dev-bin/docker_test.sh
```

**Customized**

```
dev-bin/docker_test.sh -v t/300-Exception.t
```